const person = {
    name   : 'Max',
    age    : 30,
    hobbies: ['Sports', 'Cooking'],
    greet  : function() {
        alert('Hi There');
    }
}

// person.greet();
person.age = 31  // modifying object
delete person.age  //deleting object
person.isAdmin = true  //adding object 

// console.log('person', person)

//accessing strings key
let person2 = {
    'first name': 'fajar',
    1.5         : 'heloo'
}

console.log('first name', person2['first name']); //using square bracket
console.log('1.5', person2[1.5]); //using square bracket

//next level dynamic square bracket in object
const userChosenKeyName = 'level';
let person3 = {
    [userChosenKeyName] : '....'
}
console.log('person3', person3);
