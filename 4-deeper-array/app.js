//creating arrays

const numbers = [1,2,3];
// console.log('numbers', numbers);

// const number2 = new Array() // []
// const numbers2 = new Array(5) // only give a length, special behaviour
const numbers2 = new Array(5, 2) // only give a length, special behaviour
// console.log('numbers2', numbers2);

const numbers3 = Array.of(1,2,3);
// console.log('numbers3', numbers3);

// in common way we are not uses new Array and Array.of snippets
//instead of arrays = [ 1,2,3,...]

const listItems = document.querySelectorAll('li');
// console.log('listItems', listItems);

// const arrayItems = Array.from('hi!'); // arrayItems = ['h', 'i', '!']
const arrayItems = Array.from(listItems); //create an array separated from input
// console.log('arrayItems', arrayItems);


const personalData  = [30, 'Max', {moreDetail: []}];  //can be stored for any data types
const analyticsData = [ [1, 1.6], [-5.4, 2.1] ];      //can be nested arrays of array

for ( const data of analyticsData) {
    for ( const dataPoint of data) {
        // console.log(dataPoint);
    }
}

// console.log(personalData[1], '\n');


//// push(), pop(), shift(), unshift()
const hobbies       = ['Cooking', 'Sports'];
hobbies.push('Reading'); //inserts new elements at the end of an array.
hobbies.unshift('Coding'); //inserts new elements at the start of an array.
const poppedValue = hobbies.pop(); //Removes the last element from an array and returns it.
hobbies.shift(); //Removes the last element from an array and returns it.
// console.log('hobbies1', hobbies);
// console.log('poppedValue', poppedValue);


hobbies[1] = 'Coding'; //replace array
// console.log('hobbies2', hobbies);


//// S P L I C E
hobbies.splice(1, 0, 'Good Food');
// console.log('hobbies splice', hobbies);

// hobbies.splice(0); ////remove all element arrays
// console.log('hobbies splice2', hobbies);

hobbies.splice(-1, 1, 'hiking'); ////negative indexes will start from the right
// console.log('hobbies splice3', hobbies);

//SPLICE and SLICE is different

//// S L I C E
//selecting ranges and copying arrays with slice
const testResults = [1, 5.3, 1.5, 10.99, -5, 10];
// const storedResults = testResults.slice(); //copying brand new array
// const storedResults = testResults.slice(0, 2); //selecting ranges array
// const storedResults = testResults.slice(-5, -2); //negative indexes, start from the left, and end to the right
const storedResults = testResults.slice(2); //selecting ranges after index array

testResults.push(5.91);

// console.log('storedResults', storedResults);
// console.log('testResults', testResults);


//// C O N C A T
//adding arrays to arrays
const testConcat   = [1, 5.3, 1.5, 10.99, -5, 10];
const storedConcat = testConcat.concat(7, 9.2);

testConcat.push(6.2);

// console.log('storedConcat', storedConcat);
// console.log('testConcat', testConcat);


//// FIND & FIND INDEX
const personData   = [{ name: 'Max'}, { name: 'Manuel'}];
const testIncludes = [1, 3, 'margen', 6]
// console.log('includes', testIncludes.includes('margen'));

// console.log('personData', personData.indexOf({name: 'Manuel'})); //its not a reference data

const manuel = personData.find((person, idx, persons) => {
    return person.name === 'Manuel';
})

// console.log('manuel', manuel, personData);

const maxIdx = personData.findIndex((person, idx) => {
    return person.name === 'Max'
})

// console.log('maxIdx', maxIdx, 'personData', personData[maxIdx] );


//// ForEach & Map
const prices          = [10.99, 5.99, 3.99, 6.59];
const tax             = 0.19;
// const taxAdjustPrices = [];

// for (const price of prices) {
//     taxAdjustPrices.push(price * (1 + tax));
// }

// prices.forEach((price, idx, prices) => {
//     const priceObj = { idx, taxAdjtPrices : price * (1 + tax)}
//     taxAdjustPrices.push(priceObj);
// })

const taxAdjustPrices = prices.map((price, idx, prices) => {
    const priceObj = { idx, taxAdjtPrices : price * (1 + tax)}
    return priceObj
})

// console.log(taxAdjustPrices);
console.log(prices, taxAdjustPrices);




