//Object Literal
const circle = {
    radius: 1,
    draw  : function() {
        console.log('draw');
    }
}
circle.draw();

//Factory Function
function createCircle(radius) {
    return {
        radius,
        draw: function() {
            console.log('draw');
        }
    }
}
const newCircle = createCircle(1);

//Constructor Function
function Circle(radius) {
    this.radius = radius;
    this.draw = function(){
        console.log('draw');
    }
}
const another = Circle(1);

//Primitives Values vs References/Objects values

// let number = 10 // value primitives are copied by their values
let number = { value: 10 } //reference are copied by their reference

function increase(number){
    console.log('numb1', number);
    number.value++;
    console.log('numb2', number);
}

increase(number);
console.log('number', number);
