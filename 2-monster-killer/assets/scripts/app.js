const ATTACK_VALUE         = 10;
const STRONG_ATTACK_VALUE  = 17;
const MONSTER_ATTACK_VALUE = 14;
const HEAL_VALUE           = 20;
const enteredValue         = prompt('set hp for you and monster', '100');
const MODE_ATTACK          = 'ATTACK';
const MODE_STRONG_ATTACK   = 'STRONG_ATTACK';

const LOG_EVENT_PLAYER_ATTACK        = 'PLAYER_ATTACK';
const LOG_EVENT_PLAYER_STRONG_ATTACK = 'PLAYER_STRONG_ATTACK';
const LOG_EVENT_MONSTER_ATTACK       = 'MONSTER_ATTACK';
const LOG_EVENT_PLAYER_HEAL          = 'PLAYER_HEAL';
const LOG_EVENT_GAME_OVER            = 'GAME_OVER';



function getMaxValues() {
    let parsedValues = parseInt(enteredValue);
    if(isNaN(parsedValues) || parsedValues <= 0){
        throw {message: 'invalid user input'}
    }
    return parsedValues
}

let chosenMaxLife;
try {
    chosenMaxLife = getMaxValues();
} catch (error) {
    console.log('error', error);
    chosenMaxLife = 100;
}

let currentMonsterHealth = chosenMaxLife;
let currentPlayerHealth  = chosenMaxLife;
let hasBonusLife         = true
let battleLog            = [];
let lastLoggedEntry;

adjustHealthBars(chosenMaxLife);

function reset(){
    currentPlayerHealth = chosenMaxLife;
    currentMonsterHealth = chosenMaxLife;
    resetGame(chosenMaxLife);
}

function endRound(){
    let   initialPlayerHealth  = currentPlayerHealth;
    const playerDamage         = dealPlayerDamage(MONSTER_ATTACK_VALUE);
          currentPlayerHealth -= playerDamage

    writeToLog(
            LOG_EVENT_MONSTER_ATTACK, 
            playerDamage, 
            currentMonsterHealth, 
            currentPlayerHealth
    )
    
    if(currentPlayerHealth <= 0 && hasBonusLife){
        hasBonusLife = false;
        removeBonusLife();
        currentPlayerHealth = initialPlayerHealth;
        setPlayerHealth(initialPlayerHealth);
        alert('you saved by bonus life');
    }
    
    if(currentMonsterHealth <= 0 && currentPlayerHealth > 0){
        alert('You Win');
        writeToLog(
            LOG_EVENT_GAME_OVER, 
            'Player Won', 
            currentMonsterHealth, 
            currentPlayerHealth
    )
    } else if( currentPlayerHealth <= 0 && currentMonsterHealth > 0) {
        alert('You Lost!');
        writeToLog(
            LOG_EVENT_GAME_OVER, 
            'Monster Won', 
            currentMonsterHealth, 
            currentPlayerHealth
    )
    } else if(currentMonsterHealth <= 0 && currentPlayerHealth <= 0){
        alert('draw');
        writeToLog(
            LOG_EVENT_GAME_OVER, 
            'A Draw', 
            currentMonsterHealth, 
            currentPlayerHealth
    )
    }

    //reset
    if( currentMonsterHealth <= 0 || currentPlayerHealth <= 0){
        reset();
    }
}

function attackMonster(mode) {
    const maxDamage = mode === 'ATTACK' ? ATTACK_VALUE : STRONG_ATTACK_VALUE;
    const logEvent  = mode === 'ATTACK' ? LOG_EVENT_PLAYER_ATTACK : LOG_EVENT_PLAYER_STRONG_ATTACK;
 
    const damage                = dealMonsterDamage(maxDamage);
          currentMonsterHealth -= damage;
    writeToLog(
        logEvent, 
        damage, 
        currentMonsterHealth, 
        currentPlayerHealth
    )
    endRound();
}

function attackHandler() {
    attackMonster(MODE_ATTACK);
}

function attackStrongHandler() {
    attackMonster(MODE_STRONG_ATTACK);
}

function healingHandler() {
    let healValue;
    if(currentPlayerHealth >= chosenMaxLife - HEAL_VALUE){
        alert('You can\'t heal more');
        healValue = chosenMaxLife - currentPlayerHealth; // 0
    } else {
        healValue = HEAL_VALUE;
    }
    writeToLog(
        LOG_EVENT_PLAYER_HEAL, 
        healValue, 
        currentMonsterHealth, 
        currentPlayerHealth
    )
    increasePlayerHealth(healValue); //only update progress bar ui
    currentPlayerHealth += healValue; //update value current health
    endRound();
}

function writeToLog(ev, val, monsterHealth, playerHealth) {
    let logEntry = {
        event             : ev,
        value             : val,
        finalMonsterHealth: monsterHealth,
        finalPlayerHealth : playerHealth
    };

    switch(ev) {
        case LOG_EVENT_PLAYER_ATTACK : 
            logEntry.target = 'Monster';
            break;
        case LOG_EVENT_PLAYER_STRONG_ATTACK : 
            logEntry.target = 'Monster';
            break;
        case LOG_EVENT_MONSTER_ATTACK : 
            logEntry.target = 'Player';
            break;
        case LOG_EVENT_PLAYER_HEAL : 
            logEntry.target = 'Player';
            break;
        case LOG_EVENT_GAME_OVER : 
            logEntry
            break;
        default: 
            logEntry = {}
    }
    battleLog.push(logEntry);
}

function printLogHandler() {
    // console.log(battleLog);

    let j = 0 ;
    outerWhile: do {
        console.log('Outer', j);
        innerFor: for(let k = 0; k < 5; k++){
            if(k === 3){
                break outerWhile;
                // continue outerWhile; //makes infinite loop
            }
        }
        console.log('Inner', k);
        j++
    } while(j < 3);
    
    let i = 0;
    for( const logEntry of battleLog) {
        if(!lastLoggedEntry && lastLoggedEntry !== 0 || lastLoggedEntry < i){
            for (const key in logEntry){
                console.log(`${key} : ${logEntry[key]} \n`)
            }
            lastLoggedEntry = i;
            break
        }
        i++
        
    }
}

attackBtn.addEventListener('click', attackHandler);
strongAttackBtn.addEventListener('click', attackStrongHandler);
healBtn.addEventListener('click', healingHandler);
logBtn.addEventListener('click', printLogHandler)