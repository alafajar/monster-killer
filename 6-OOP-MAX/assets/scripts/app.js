class Product {
    // class field, ex:
    // title = 'DEFAULT';

    constructor(title, img, desc, price){ //class property
        this.title       = title;
        this.imageUrl    = img;
        this.description = desc;
        this.price       = price;
    }

    render(){} //class method
}

class ShoppingCart {
    item = [];

    constructor() {}

    addProduct(product){
        this.items.push(product);
        this.totalOutput = `<h2>Total : \$${1}</h2>`
    }

    render(){
        const cartEl = document.createElement('section');
        cartEl.innerHTML = `
            <h2>Total : \$${0}</h2>
            <button>Order Now!</button>
        `;
        cartEl.className = 'cart';
        return cartEl;
    }
}

class ProductItem {
    constructor(product) {
        this.product = product;
    }

    addToCart() {
        console.log('adding to cart');
        console.log(this.product);
        
    }

    render() {
        const prodEl           = document.createElement('li');
        const prod             = this.product
              prodEl.className = 'product-item';
              prodEl.innerHTML = `
                <div>
                    <img src="${prod.imageUrl}" alt="${prod.title}/>
                    <div class="product-item__content">
                        <h2>${prod.title}</h2>
                        <h3>\$${prod.price}</h3>
                        <p>${prod.description}</p>
                        <button>Add to Cart Button</button>
                    </div>
                </div>
            `;
        const addCartButton = prodEl.querySelector('button');
        addCartButton.addEventListener('click', this.addToCart.bind(this)); 
        return prodEl;
    }
}

class ProductList {
    products = [
        new Product(
            'A Pillow',
            'https://www.shopmarriott.com/images/products/v2/lrg/Marriott-The-Marriott-Pillow-MAR-108-L_lrg.jpg',
            'A soft pillow',
            19.99
        ),
        new Product(
            'A Carpet',
            'https://images-na.ssl-images-amazon.com/images/I/81W6An71HSL._SL1500_.jpg',
            'A good carpet',
            89.99
        ),
    ];

    constructor() {}

    render() {
     
        const prodList           = document.createElement('ul');
              prodList.className = 'product-list';
        for (const prod of this.products){ //iterate 'of' for array
            const productItem = new ProductItem(prod);
            const prodEl      = productItem.render();
            prodList.append(prodEl);
        }
       return prodList;
    }
}

class Shop {
    constructor() {}

    render(){
        const renderHook  = document.getElementById('app');
        const cart        = new ShoppingCart();
        const cartEl      = cart.render();
        const productList = new ProductList();
        const prodListEl  = productList.render();

        renderHook.append(cartEl);
        renderHook.append(prodListEl);
    }
}

const shop = new Shop();
shop.render();
