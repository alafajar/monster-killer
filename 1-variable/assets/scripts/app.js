const defaultResult = 0;
let   currentResult = defaultResult;
let   logEntries    = []

function getUserNumberInput () {
    return parseInt(userInput.value);
}

function calculateResult(calculationType) {
    
    const enterNumber    = getUserNumberInput();
    if (
        calculationType !== 'ADD' &&
        calculationType !== 'SUBTRACT' &&
        calculationType !== 'MULTIPLY' &&
        calculationType !== 'DIVIDE' ||
        !enterNumber //truthy or falsy values
    ) {
        return;
    }
        const initialResult  = currentResult;
        let mathOperator;
        if( calculationType === 'ADD'){
            currentResult += enterNumber;
            mathOperator   = '+'
        } else if(calculationType === 'SUBTRACT') {
            currentResult -= enterNumber;
            mathOperator   = '-'
        } else if(calculationType === 'DIVIDE') {
            currentResult /= enterNumber;
            mathOperator   = '/'
        } else if(calculationType === 'MULTIPLY') {
            currentResult *= enterNumber;
            mathOperator   = '*'
        } 
        createAndWriteLog(mathOperator, initialResult, enterNumber );
        writeToLog(calculationType, initialResult, enterNumber, currentResult);
}


function createAndWriteLog(operator, resultBefore, calcNumber) {
    const calcDescription = `${resultBefore}  ${operator} ${calcNumber}`;
    outputResult(currentResult, calcDescription);
}

function writeToLog(operation, prevResult, number, result ){
    const logEntry = {
        operation ,
        prevResult,
        number    ,
        result    ,
    }
    logEntries.push(logEntry);
    console.log(logEntries);
}


addBtn.addEventListener('click', calculateResult.bind(this, 'ADD'))
subtractBtn.addEventListener('click', calculateResult.bind(this, 'SUBTRACT'))
multiplyBtn.addEventListener('click', calculateResult.bind(this, 'DIVIDE'))
divideBtn.addEventListener('click', calculateResult.bind(this, 'MULTIPLY'))



