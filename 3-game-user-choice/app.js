const startGameBtn = document.getElementById('start-game-btn'); 

const ROCK                = 'ROCK';
const PAPER               = 'PAPER';
const SCISSORS            = 'SCISSORS';
const DEFAULT_USER_CHOICE = ROCK;

const RESULT_DRAW   = 'DRAW';
const PLAYER_WINS   = 'Player wins';
const COMPUTER_WINS = 'Computer wins';

let gameIsRunning = false;

const getPlayerChoice = () => {
    const selection = prompt(`${ROCK}, ${PAPER} or ${SCISSORS}?`, '').toUpperCase();
    if (
        selection !== ROCK &&
        selection !== PAPER &&
        selection !== SCISSORS
    ) {
        alert(`Invalid choice! We chose ${DEFAULT_USER_CHOICE} for you!`);
        return;
    }
    return selection;
};

const getComputerChoice = () => {
    let randomValue = Math.random();
    if(randomValue === 0.34){
    return ROCK;
    } else if (randomValue === 0.67){
    return SCISSORS;
    } else {
    return PAPER;
    }
}

const getWinner = (cChoice, pChoice = DEFAULT_USER_CHOICE ) => {
    if(cChoice === pChoice) {
        return RESULT_DRAW
    } else if(
        cChoice === ROCK && pChoice === PAPER ||
        cChoice === SCISSORS && pChoice === ROCK ||
        cChoice === PAPER && pChoice === SCISSORS 
    ) {
        return PLAYER_WINS;
    } else {
        return COMPUTER_WINS;
    }
}

startGameBtn.addEventListener('click', () => {

    if(gameIsRunning){ return; };

    gameIsRunning = true;
    console.log('Game is starting...');
    const playerChoice   = getPlayerChoice();
    const computerChoice = getComputerChoice();
    let winner; 
    if(playerChoice){
        winner         = getWinner(computerChoice, playerChoice);
    } else {
        winner         = getWinner(computerChoice);
    }
    let message = `${ playerChoice || DEFAULT_USER_CHOICE} vs ${computerChoice}`
    if(winner === RESULT_DRAW){
        message = message + ' is draw' 
    } else if(winner === PLAYER_WINS){
        message = message + ' is player wins'
    } else {
        message = message + ' is computer wins'
    }
    alert(message)
    console.log('winner', winner)
});

const combine = (resultHandler, operation, ...numbers) => {
    const validateNumber = (number) => {
        return isNaN(number) ? 0 : number;
    }
    let sum = 0;
    for (const num of numbers) {
        if( operation === 'ADD'){
            sum += validateNumber(num);
        } else {
            sum -= validateNumber(num);
        }
    }
    resultHandler(sum);
}

// const subtractUp = function(resultHandler, ...numbers) {
//     let sum = 0;
//     for (const num of numbers){ //dont use that
//         sum -= num;
//     }
//     resultHandler(sum);
// }

const showResult = (msgTxt, result) => {
    
    alert(msgTxt + ' ' + result);
}

// console.log(sumUp(showResult, 1,3,-5, 'ciduk', 12));
combine(showResult.bind(this, 'got result sum'), 'ADD' ,1,3,-5, 'ciduk', 12);
combine(showResult.bind(this, 'got result subtract'), 'SUBTRACT', 1, 10, 15, 20);

// 'got result '